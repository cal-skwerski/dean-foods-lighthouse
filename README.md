# README #

### What is this repository for? ###

* This project is a boilerplate starting point for building a new Oliver Wyman Lighthouse. The was originally for workers' compensation, but now can be used for any coverage.
* Version 1.05.01

Please skim all the content below before starting. If you are just filling out setup.xlsx and not running R, ignore the segments about R, RStudio, and the R Shiny server.

### Prerequisites ###

Complete the following before using these files, if you haven't already:

* Install R [https://www.r-project.org/]
* Install RStudio [https://www.rstudio.com/products/rstudio/download/] (Desktop, Open Source version, 64-bit is better).
* Install our custom R package owactools (see https://bitbucket.org/owac/owactools).
* Install the developer version of the highcharter R package via this line of code: devtools::install_github("jbkunst/highcharter").

### Timeline ###

There are a few key stages of building and launching a dashboard. Estimated time is 12 to 30 business days, so try to leave at least a month and a half.

* **Gather Loss Runs** 1 to 4 weeks - Getting historical and current loss runs from the client. They must include required fields mentioned below. Timeline can vary quite a bit since it depends on when the client can send the data. If you already have the loss runs, this step will be quick.
* **Complete setup.xlsx** 1 to 3 days - Go through each tab of setup and fill it out. The most difficult part is gathering AvE and Projected Ultimates (both optional) data from prior analyses.
* **Run dataPrep.R** 2 to 4 days - If the data is in a standard format and the code runs without bugs/errors, this will be quick. But leave extra time in planning for unexpected errors as they often occur. Contact Bryce Chamberlain [bchamberlain@oliverwyman.com] if you come across any errors you aren't able to resolve.
* **Launch and Test App Locally** 2 days - The app usually launches very smoothly once dataPrep runs without error, now is the time to check the numbers against the base dataset and make sure they are correct.
* **Publish the app to the server** 2 days - Load the app to the server and verify that it works. Request logins from IT and share the link with your client.

### Building and Testing Locally ###

* From this page, go to Downloads > Download repository and save into the folder where you'll be working.
* Replace app/www/logo.png with the client's logo, using the same file name and type/extension (logo.png). Try to find a logo with a clear background and minimal white space at the edges, I like to crop them with SnagIt Editor.
* Put your loss runs into the loss-runs folder. Each file should have the evaluation date in the file name, in any date format. This is where the code gets evaluation date from.
* Fill out the setup.xlsx file. There are a number of items here that may take a lot of work: Exposures, Column Names, Actual vs. Expected, Projected Ultimates. Check out the code in the col-maps folder for assistance in building column name maps. It is very new and not well organized, but may be helpful.
* Run dataPrep.R in RStudio (open the file, ctrl + alt + R ). Once this finishes, it'll save source.RData and other data files into your app/data/ folder which contains all the data the app needs. Contact Bryce Chamberlain [bchamberlain@oliverwyman.com] if you come across any errors you aren't able to resolve.
* Run the Shiny app (open app/global.R in RStudio and click Run App) to verify it works as expected.

### File Format for Current and Historical Loss Runs ###

Certain fields are required for the dashboard. They don't need to be named a certain way or come in a specific format. Instead, the field simply needs to be there in some form. 

These fields are required in all loss runs (both current and historical):

* **Claim Number** Must be unique per evaluation.
* **Total Paid**
* **Total Incurred**

Other fields come from the current loss run. These are required in the current loss run only:

* Loss Date
* Coverage (Workers Comp, Auto Liability, etc.)
* State Abbreviation (for plotting metrics geographically)
* Business Unit (or similar breakdown, used on Geographic tab)
* Location (or similar sub-unit of Business Unit, used on Geographic tab)

Remaining fields are strongly recommend if you can get them, but are not necessary to build a dashboard.

* Claim Type
* Body Part
* Nature of Injury
* Cause of Injury
* Legal Status
* Claim Status

You can also bring in your own new fields if you'd like to use them as filters. Set them up in the setup.xlsx sheets Filters and Columns to do so.

Exposure data are not required but are strongly recommended if you can get them, and in as much detail as possible.

### Actual vs. Expected, Ultimate Projects ###

* AvE (optional) is typically included (see Actual vs. Expected sheet) and contains results from actuarial analysis.
* Include Projected Ultimates (optional) data if requested by the project leader (also in setup.xlsx).

### Moving to Server ###

If you have access to put files on our R Shiny server, you can follow these steps. To get access, contact John Brodoff John.Brodoff@oliverwyman.com. Otherwise, contact [bryce.chamberlain@oliverwyman.com](mailto:bryce.chamberlain@oliverwyman.com) for help publishing your app.

**Prerequisite: Install and Set Up WinSCP**

* Download WinSCP from [https://winscp.net/eng/download.php](https://winscp.net/eng/download.php).
* Save the install file and use Right-click > Install/Run Elevated. If you don't have that option, try just opening the install file without any special options.
* Install and open WinSCP.
* Create a new connection with options:
    * File Transfer Protocol: SFTP
    * Host Name: usdfw21as164
    * Port: 22
    * Username and password provided by ITS for the R Shiny server.
* Login, you should now see files and have access to the folders mentioned below.

**Publish Your Lighthouse**

* I recommend creating three versions: dev (your use only), test (for your testers), and production (for the client). That way you can break your dev version (things tend to break when making changes) without affecting testing or production, etc. Once you are sure your code runs on the server, you can put the files in the production folder.
* For testing and dev, if you create a new client folder at /data/shiny/actuarial/internal-apps/dev/lighthouse/ or /data/shiny/actuarial/internal-apps/test/lighthouse/ and put your files from app/ there, it should work.
* The URL for a file at /data/shiny/actuarial/internal-apps/dev/lighthouse/acompany will be https://oliverwymanapps.mmc.com/actuarial/internal-apps/dev/lighthouse/acompany, for example.
* For production, create a folder at /data/shiny/actuarial/lighthouse/ with the name of your client and put your files from app/ there.
    * If you put it in the lighthouse folder it should work. If it doesn't, you may need to point it to the correct R Library.
    * Ask John Brodoff John.Brodoff@oliverwyman.com to point your app to:
        * r_path "/opt/microsoft/ropen/3.4.1/lib64/R/bin/R"; exec_supervisor "R_LIBS_USER=/data/shiny/actuarial/lib/Shiny_DB/lib3.4.1";
        * Provide him with the line above and the URL of your app. He'll know what to do.
        * John can also set up access for your clients. This process is usually very fast but can take up to 2 business days, so make sure to leave enough time for any unexpected hiccups.

*General Shiny Best-Practice:*

* For any Shiny app, please add a readme.txt with your contact information and any other information you think might be helpful to someone coming across your app.
* Internal apps should be placed within the /data/shiny/actuarial/internal-apps/ directory. This protects external, client-facing apps from being brought down temporarily if there is high traffic on internal apps like test, dev, and internal tools.
* Sometimes the server takes a few seconds to make the files available to your app. If your app doesn't work the first time you access it after making a change, refresh it a few times and it might fix itself.

### Who do I talk to? ###

* Bryce Chamberlain [bryce.chamberlain@oliverwyman.com](mailto:bryce.chamberlain@oliverwyman.com)