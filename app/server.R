# File owner: Bryce chamberlain
if (!interactive()) sink(stderr(), type = "output") # necessary to print() to the log file when on the server. https://stackoverflow.com/questions/33912150/how-can-i-get-standard-output-written-to-a-log-file-when-a-shiny-app-is-deployed

function (input, output, session) {

  # Write user information.
    if(is_local){
      iuser = 'local.developer@oliverwyman.com'
    } else {
      iuser = session$user
    }
    usertype = ifelse( grepl('@oliverwyman.com$',iuser), 'owac', 'other' ) # save globally for reference later.
    iuser = data.frame(
      date = as.character(lubridate::today()),
      datetime = as.character(lubridate::now()),
      user = iuser,
      domain = gsub('^[^@]+@([^@]+$)','\\1',iuser),
      usertype = usertype,
      dev = ifelse(grepl( '(casey.lilek)|(bryce.chamberlain)', iuser,ignore.case = TRUE ), 'dev', ''),
      version_num = uiinf$version_num,
      dashboard = getwd(),
      stringsAsFactors = FALSE
    )
    tryCatch({ user_log = read.any( 'user_log.csv', auto_convert_dates = FALSE ) },error=function(e){},warning=function(w){})
    if(exists('user_log')){ user_log = bind_rows(user_log,iuser) } else { user_log = iuser }

    # Try to write the file. I'm a bit concerned that this will case an error if the user doesn't have write permission, so use tryCatch.
    tryCatch({ w(user_log,'user_log') }, error=function(e) warning(e), warning=function(w) warning(w) ) # Use warning so it prints to log, but don't stop the code.


  # Load general server Functions
  for( i in list.files( 'server/functions', recursive = TRUE ) )  if( grepl( '[.][Rr]$', i ) ) source( paste0( 'server/functions/', i ), local = TRUE )$value # https://github.com/daattali/advanced-shiny/blob/master/split-code/app.R
  
  # Load the other server files.
  for(i in list.files( 'server', recursive = TRUE ) ) if( grepl( '[.][Rr]$', i )  && !grepl( '/functions', i ) ) source( paste0( 'server/', i ), local = TRUE )$value # https://github.com/daattali/advanced-shiny/blob/master/split-code/app.R

  # Set up download of user info.
  if( usertype == 'owac' ){
    output$user_log_download <- downloadHandler(
        filename = "user_log.csv",
        content = function(file) owactools::w(user_log,file)
    )
  }

  # Housekeeping. Variables defined here will flow through the rest of the app.
  rm(user_log,iuser,usertype)

  # Initialize variables in this session which will be used throughout but need to be user-specific, not global.
  workdt = initdata # this gives us a non-global data to work with, so changes to data don't affect other apps or on refresh.
    
}