output$launch_topclaims_move_inc <- renderHighchart({ launch_topclaims_chart('prior_incDiff') })
output$launch_topclaims_new_inc <- renderHighchart({ launch_topclaims_chart('totInc', height = uiinf$launch_top_chartHeight + 16 ) })
output$launch_topclaims_move_pd <- renderHighchart({ launch_topclaims_chart('prior_pdDiff') })

# Create point format string from inputs.
launch_pointFormat <- '<span style="font-weight:bold; font-family:\'Rubik\';">${point.y:,.0f}</span>' # start.
for( i in 1:nrow(sidebarInputs) ){
    icol = sidebarInputs$api_name[i]
    if(icol=='state') icol = 'state_abbr' # we rename state to something else so we can use piont.state here. In highcharts, point.state is hover/selected/etc.
    ilabel = sidebarInputs$label[i]
    launch_pointFormat %<>% paste0('<br/>',ilabel,' <strong>{point.', icol, '}</strong>')
}

launch_top_movers <- function(ilaunch_line){ 
    
    l('launch_top_movers')

    idt = ilaunch_line %>% filter( eval == currentEval )

    ilaunch_top_movers = list(
        move_pd = idt %>% top_n( 10, prior_pdDiff ) %>% arrange( desc(prior_pdDiff) ) %>% mutate( label = claimNum ),
        move_inc = idt %>% top_n( 10, prior_incDiff ) %>% arrange( desc(prior_incDiff) ) %>% mutate( label = claimNum ),
        new_inc = idt %>% filter( is.na(prior_totInc) ) %>% top_n( 10, totInc ) %>% arrange( desc(totInc) ) %>% mutate( label = paste0( claimNum, ' @ ', gformats$date$formatter(lossDt) ) )
    )

    return(ilaunch_top_movers)

}

# Chart of top claims.
launch_topclaims_chart <- function( imetric_colname, height = uiinf$launch_top_chartHeight ){

    launch_updateAll()
    if(nanull(workdt[[coverage()]]$launch$top_movers)) return(NULL)
    iclaims = workdt[[coverage()]]$launch$top_movers[[ switch( imetric_colname, 'prior_pdDiff' = 'move_pd', 'prior_incDiff' = 'move_inc', 'totInc' = 'new_inc'  ) ]]

    # dynamically calculate left margin based on length of the label.
    imaxlength = max(nchar(iclaims$label))
    imarginLeft = min( 200, 125 + 100*max(imaxlength/15-1,0) )
    ilables = c(
        'prior_pdDiff' = 'Change in Paid',
        'prior_incDiff' = 'Change in Incurred',
        'totInc' = 'Total Incurred'
    )

    ichart <- highchart() %>%
        hc_chart( 
            height = height, 
            width = 320,
            marginLeft = imarginLeft, marginRight = 0 
        ) %>%
        hc_xAxis( 
            categories = iclaims$label, 
            labels= list( 
                    style = list( fontSize ='8pt', fontFamily = 'OpenSans', enabled = TRUE ), 
                    enabled = TRUE
                ),
            #scrollbar = list( enabled = TRUE ),
            #max = 10,
            title = list( enabled = TRUE )
        ) %>%
        hc_yAxis( labels = list( enabled = FALSE ), gridLineDashStyle = NULL, gridLineWidth = 0  ) %>%
        hc_plotOptions(
            series = list(
                animation = FALSE,
                stickyTracking = FALSE,
                groupPadding = .1,
                pointPadding = 0
            ),
            line = list( cursor = "ns-resize" )
        ) %>% 
        hc_legend( enabled = FALSE )

    # Dynamic hover not used for this, but leaving this here in case we want to bring it in later.
    
    # we need to rename state to something else so we can use piont.state here. In highcharts, point.state is hover/selected/etc.
    idt <- iclaims[, c( imetric_colname, 'claimNum', "lossDt", 'label', sidebarInputs$api_name ) ]
    colnames(idt)[colnames(idt)=='state'] <- 'state_abbr'
    colnames(idt)[1] <- 'y' # metric must be called y for the chart to work.

    ichart %<>% hc_add_series( 
        data = list_parse(idt), type = "bar", name = ilables[[imetric_colname]],
        color = mmc_colors['sapphire','bright'], 
        tooltip = list( pointFormat = paste0('{series.name} ',launch_pointFormat) ),
        dataLabels = list(
            formatter = JS('chartLabel'),
            enabled = TRUE, inside = TRUE, align = 'right', verticalAlign = 'bottom',
            padding = 2,
            style = list( 
                fontWeight = 'normal', fontFamily = 'Rubik', 
                color = 'White',
                fontSize = '9pt', textOutline = '0px'
            )
        )
    )

    return(ichart)

}