getByClaimYear <- function( idt, iminReportedCount, icolname = NULL ){

  idt %<>% applyFilters_maturity(suppressPrint = TRUE) %<>% setPrior()
  maxyr = as.numeric( max( idt$axYr ) ) # this will change based on the selected maturity.
  idt %<>% filter( as.numeric(axYr) >= (maxyr - 3) ) # take up to the most recent 3 years.

  # for melt, we need an id column, so create one and group by it so it comes through.
  isfiltered = TRUE
  if(nanull(icolname)){
    isfiltered = FALSE
    icolname = 'overall'
    idt[[icolname]] <- 'overall'
  }
  
  # Maturity stuff.
  imat = isolate(input$maturity)
  imattext = tolower(imat)
  if(imattext!='current evaluation') imattext = paste0(imattext,' maturity')
  
  idt %<>%

    # Group and summarize with exposure type and axYr so we can attempt to add exposure.
    group_by_at( c( icolname, 'exposureType', 'axYr' ) ) %>%
    summarise_at( vars( totPd, totInc, clsCnt, repCnt, prior_clsCnt ), funs(sum), na.rm = TRUE ) %>% ungroup() %>%

    # Attempt to add exposures.
    gen_addExposure(imat) %>%

    # Now group and summarize by only the selected column.
    group_by_at( c( icolname, 'axYr' ) ) %>% sumNumeric() %>% 

    mutate(
      incSev = totInc/repCnt,
      pdSev = ifelse( clsCnt == 0, NA, totPd/clsCnt ),
      clsRate = clsCnt/repCnt,
      incrClsRate = ifelse( repCnt - prior_clsCnt == 0, NA, (clsCnt - prior_clsCnt) / (repCnt - prior_clsCnt) ),
      openAndNewlyClosed = (repCnt - prior_clsCnt)
    ) %>% 

    # If we have exposure, get exposure-based metrics.
    exposureMetrics()

    # Melt the dataset so we have a list of metrics instead of columns of metrics.
    idt %<>% select_at( intersect(
      c(icolname, 'axYr', 'repCnt', 'clsCnt', 'openAndNewlyClosed', 'incSev', 'pdSev', 'clsRate', 'incrClsRate', 'Incurred Loss Rate', 'Paid Loss Rate', 'Reported Frequency' ),
      colnames(idt)
    )) %>%
    melt(id = 1:5) %>% # this covert to long form for better identification.
    mutate( 
      variable = as.character(variable) # this comes out as a factor which causes warnings and binding issues later.
    )

    # Add longitudinal metrics if this is not overall.
    if( isfiltered ){
      
      # Restrict to only the latest 2 years.
      priorYr = as.numeric(endYr)-1
      idt %<>% filter( axYr %in% c(priorYr,endYr) )
      
      # Get easy trend metrics.
      idt_trend = idt %>% group_by_at( c(icolname, 'variable') ) %>% summarize( 
        latest = sum( if_else( axYr == endYr, value, 0 ) ),
        last = sum( if_else( axYr == priorYr, value, 0 ) )
      ) %>% mutate(
        pctchg = (latest-last)/last,
        pctchg_abs = abs(pctchg)
      )
      
      # Join them to the data and reduct to only the latest year.
      idt %<>% left_join( idt_trend, by = c(icolname,'variable') ) %>% filter( axYr == endYr )
      
      rm( priorYr, idt_trend ) # Housekeeping.

    # Otherwise just filter to the latest year.
    } else { idt %<>% filter( axYr == maxyr ) %>% select( -axYr ) }

    # Set cohort label and size, but only for filterd.
    if( isfiltered ){

      idt %<>% mutate( cohort_size = NA, cohort_label = NA)

      imets = list(
        incSev = list( cohort_col = 'repCnt', cohort_label = 'claims reported' ),
        pdSev = list( cohort_col = 'clsCnt', cohort_label = 'closed claims' ),
        clsRate = list( cohort_col = 'repCnt', cohort_label = 'claims reported' ),
        incrClsRate = list( cohort_col = 'openAndNewlyClosed', cohort_label = 'claims open and newly closed' ),
        `Incurred Loss Rate` = list( cohort_col = 'repCnt', cohort_label = 'reported claims' ),
        `Paid Loss Rate` = list( cohort_col = 'clsCnt', cohort_label = 'closed claims' ),
        `Reported Frequency` = list( cohort_col = 'repCnt', cohort_label = 'reported claims' )
      )

      for( i in names(imets)){
        idt$cohort_size[ idt$variable == i ] <- idt[ idt$variable == i, imets[[i]]$cohort_col ] # set the column.
        idt$cohort_label[ idt$variable == i ] <- imets[[i]]$cohort_label # set the label.
      }
      
      # Add output columns. These only go on filtered so they don't get duplicated in joins.
      idt$notes <- paste0( 'During ', idt$axYr, ' @ ', imattext )
      idt$type <- 'byClaimYear'
      
    }
  
  return(idt)

} 