byPeriod_namefix <- function( inames ) gsub( '^ 0%$', 'No Change', gsub( "x", "%", gsub( "chg", "", gsub("_", " ", inames ) ) ) )

byPeriod_incChange <- function(){
  
  idt = byPeriod$incChangeGraph
  
  # Summarize chart.
  idt %<>% 
    rename_(stat = switch( input$byPeriod_incChangeStat, "Counts" = "obs", "Dollars" = "amt" ) ) %>%
    mutate( stat = abs(stat) ) %>%
    select( label, prior_change_bucket, stat ) %>%
    spread( key = prior_change_bucket, value = stat ) %>% 
    #mutate( Total = rowSums(.[1:nrow(.), 2:ncol(.)], na.rm = TRUE ) ) %>% 
    ungroup() %>% data.frame() %>% arrange( lubridate::mdy(gsub('^.+[^0-9/]([0-9/].+)$','\\1',label)) ) %>%
    tail(max_eval_dates_to_show[ annual_quarterly() ])
  
  idt[ is.na(idt) ] <- 0

  # Sort the buckets by labels.
  idt <- idt[, c( 'label', intersect(labels_pctChange,colnames(idt)) ) ]

  # Fix column data types to prevent downstream errors.
  idt %<>% mutate( label = as.character(label) )

  return(idt)

}

output$byPeriod_incChangeGraph <- renderHighchart({
  
  # this chart is problematic so redraw it when the trend tab is changed.
 # t = input$trend_tabset 
  #t = input$tabset
  l('drawing byPeriod_incChangeGraph')
  
  isolate(byPeriod_updateAll()) # this will update the data if necessary. Somehow, this is making it reactive to the tab change or some sidebar input change, which breaks the chart. 
  t = applysum() # reactivity.
  
  idt <- byPeriod_incChange()
  
  # Build highchart.
  hc <- highchart() %>%
    hc_chart( height = 270, width = 982 ) %>%
    hc_legend( itemStyle = list( fontWeight = 'normal' ) ) %>%
    hc_xAxis( title = list( enabled = FALSE, text = "Period" ), categories = idt$label ) %>%
    hc_yAxis( title = list( text = "Percent" ) ) %>%
    hc_colors(rev(c('#EF4E45','#F9BEAD','#f1f3f3','#BDDDA3','#72BE44'))[ labels_pctChange %in% names(idt) ]) %>% # pick only the colors that match labels. 
    hc_plotOptions(
      series = list(animation = FALSE),
      column = list(stacking = "percent")
    ) %>%
    hc_tooltip(valueDecimals = 0,pointFormat = switch(input$byPeriod_incChangeStat,
      "Counts" = '<span>{series.name}</span>: <b>{point.y} Counts</b> ({point.percentage:.0f}%)<br/>',
      "Dollars" = '<span>{series.name}</span>: <b>${point.y}</b> ({point.percentage:.0f}%)<br/>')
    )
  
  if( length(idt) >= 2) for(i in 2:length(idt)) { # Add years.
    hc %<>% hc_add_series(
      data = idt[, i], type = "column", showInLegend = TRUE,
      name = colnames(idt)[i] %>% byPeriod_namefix()
    )
  }

  return(hc)

})

# Code for the table part of this chart.
byPeriod_incChangeTableData <- reactive({

  byPeriod_updateAll() # this will update the data if necessary.
  
  idt <- byPeriod_incChange()

  # Transpose and set the old labels as new column names.
  idt <- data.frame( t( idt ), stringsAsFactors = FALSE )
  colnames(idt) <- idt[ 1, ]
  idt <- idt[ 2:nrow(idt), ]
  
  # Convert back to numeric.
  for( i in colnames(idt) ) idt[[i]] <- as.numeric(idt[[i]]) # Numbers get converted to strings somehow.
  
  # Set categories.
  idt$Category <-rownames(idt) %>% byPeriod_namefix()
  
  idt <- dplyr::select( idt, Category,everything() ) # bring Category to the first column.

  # Set all decreases to negative numbers when totals is selected.
  if( input$byPeriod_incChangeStat == 'Dollars' ) for( i in setdiff( colnames( idt ), 'Category' ) ){
    idt[[i]][ grepl('Decrease', idt$Category ) ] <- - idt[[i]][ grepl('Decrease', idt$Category ) ]
    idt[[i]][ idt$Category == 'No Change' ] <- 0
  }
  
  return( list(
    data = idt, 
    totals = dplyr::summarise_all( select( idt, -Category ), sum )
  ))
  
})

output$byPeriod_incChangeTable <- renderDataTable({

  byPeriod_updateAll() # this will update the data if necessary.
  
  sketch <- htmltools::withTags(table(
    tableHeader( colnames( byPeriod_incChangeTableData()$data ) ),
    tableFooter( c( "Total", format( as.numeric( byPeriod_incChangeTableData()$totals[ 1, ] ), big.mark = ",", trim = TRUE, digits = 0, scientific = FALSE ) ) )
  ))
  
  idtbl <- datatable(
    byPeriod_incChangeTableData()$data,
    container = sketch,
    rownames = FALSE,
    autoHideNavigation = TRUE,
    options = list( bLengthChange = 0, bFilter = 0, bInfo = 0, paging = FALSE )

  )

  idtbl %<>% formatCurrency( columns = 2:ncol( byPeriod_incChangeTableData()$data ), currency = "", digits = 0 ) # current (blank) to format as number with commas.
  
  return(idtbl)
  
})