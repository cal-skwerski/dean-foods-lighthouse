cda_table <- reactive({ lp_buildTable_table(input$cda_metric) })

lp_buildTable_table <- function( icolname ){

  idt <- lp_tableData() %>% lp_mut() %>% lp_toPct() %>%
    select_at( c( input$LP_rowLabels, input$LP_colLabels, icolname ) )  %>% 
    spread( key = input$LP_colLabels, value = icolname )
  
  # If row and column are the same, the row column will not be there. Fill it in.
  if( ! input$LP_rowLabels %in% colnames(idt) ){
    idt[[input$LP_rowLabels]] <- 'Overall'
    idt = idt[, c(input$LP_rowLabels,setdiff(colnames(idt),input$LP_rowLabels))]
  } else {
    # If this is an age bucket, re-sort the values. Only applies if we have multipl values here.
    idt %<>% resort_rows_ageGroup()
  }

  # If this is an age bucket, re-sort the values.
  idt %<>% resort_columns_ageGroup()

  return(idt)

}

lp_buildTable_col_overall <- function(icolname){
  idt <- lp_tableData() %>% group_by_at( input$LP_colLabels ) %>% lp_summ() %>% lp_mut() %>% lp_toPct() %>% select_at( c( input$LP_colLabels, icolname ) ) %>% spread( key = input$LP_colLabels, value = icolname )
  idt %<>% resort_columns_ageGroup()
  return(idt)  
}

lp_buildTable_row_overall <- function(icolname){
  
  # If row/column are the same then this is just 'Overall' and the total average.
  if(input$LP_colLabels == input$LP_rowLabels){
    idt = data.frame( var = 'Overall', metr = lp_buildTable_overall(icolname) )
    colnames(idt) <- c(input$LP_rowLabels, icolname)
    
  # Otherwise summarize and sort rows.
  } else {
    idt <- lp_tableData() %>% group_by_at( input$LP_rowLabels ) %>% lp_summ() %>% lp_mut() %>% lp_toPct() %>% select_at( c(input$LP_rowLabels, icolname) )
    idt %<>% resort_rows_ageGroup()
  }
  
  return(idt)

}
lp_buildTable_overall <- function(icolname) lp_tableData() %>% lp_summ() %>% lp_mut() %>% lp_toPct() %>% select_at(icolname)

cda_table_data <- reactive({ cda_table() %>% left_join( lp_buildTable_row_overall(input$cda_metric), by = input$LP_rowLabels ) })

cda_total_label <- function() ifelse( isolate(input$cda_metric) %in% cda_volume, 'Total', 'Overall' )

cda_metric_label <- function() for( i in 1:length(cda_metrics)) if(cda_metrics[[i]] == isolate(input$cda_metric)) return(names(cda_metrics)[i])

output$cda_table <- renderDataTable({

    idt <- cda_table_data()
    idt[is.na(idt)] <- 0
    imetric = input$cda_metric
    iheaders = c( label(), colnames(cda_table()[, -1]), cda_total_label() )

    icontainer <- htmltools::withTags(table(
      tableHeader( iheaders ),
      tableFooter( c(cda_total_label(), 
        gformats[[ifelse(lp_ispct(),'percentage','count')]]$formatter( as.numeric( lp_buildTable_col_overall(imetric)[1, ] ) ),
        gformats[[ifelse(lp_ispct(),'percentage','count')]]$formatter( as.numeric( lp_buildTable_overall(imetric) ) )
    ))))
    
    idtbl = datatable(
      idt,
      container = icontainer,
      rownames = F, extensions = "Buttons", 
      options = list(
        scrollX = TRUE, dom = 'ltip',
        columnDefs = list(
          list(orderSequence = c('desc', 'asc'), targets = "_all")
        )
      ),
    )
    if(lp_ispct()){
      idtbl %<>% formatPercentage( columns = 2:ncol(idt), digits = 2 )
    } else {
      idtbl %<>% formatCurrency( columns = 2:ncol(idt), currency = "",interval = 3, mark = ",", digits = 0 )
    }

    # Apply conditional color formatting.
    #inums = idt %>% select_if(is.numeric)
    #imin = min(inums)
    #imax = max(inums)
    #icols = c('','','',A_clrsReds)
    #lcuts = length(icols)-1

    #idtbl %<>% formatStyle( 2:ncol(idt), backgroundColor = styleInterval(
    #  cuts = as.numeric( sapply( 1:lcuts, function(i) imin + (imax-imin)/lcuts*i  ) ),
    #  values = c('','','',A_clrsReds) # use colors from actuarial.
    #))
  
    return(idtbl)
    
})

output$cda_table_download <- downloadHandler( ####Download loss statistic table ====
    filename = function() { paste0( 'Cost Driver Analysis for ', cda_metric_label(), ".csv" ) },
    content = function(file) { 
      temp <- cda_table_data()
      names(temp)[1] <- label()
      names(temp)[ncol(temp)] <- cda_total_label()
      write.csv(temp, file, row.names = FALSE, na = "0") 
    }
)

# Observers to manage visibility.
observeEvent( input$cda_metric, {
  if( input$cda_metric %in% cda_volume ){
    shinyjs::show('button_cda_pct')
  } else {
    shinyjs::hide('button_cda_pct')    
  }
})