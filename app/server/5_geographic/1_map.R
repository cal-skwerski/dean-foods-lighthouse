# Implementing reactivity for the map is a bit tricky. 
# There is a hidden input 'selectedStates' in ui.R, clearing selected states on masterData recalc in server/functions/data_claims.R, and
#   javascript updating of this input on map click implemented via www/js/map.js
output$g_map <- renderHighchart({ 
  
    proginit()

    geo_updateAll()

    idt <- geo$stateBU %>% group_by(state) %>% sumNumeric() %>% ungroup() %>% gmut() %>% filter( repCnt >= geo_minCohortCount( forTable = FALSE ) )
    
    colnames(idt)[colnames(idt)==input$g_mapStat] <- 'val'
    iformat = gformats[[ gtypes[[input$g_mapStat]] ]]
    iheight = chart_height - 53
    icolor = color.dark( colorFor('g_map') )

    ichart <- highchart() %>% hc_mapNavigation( enableMouseWheelZoom = TRUE )
    
    ichart %<>% hc_add_series_map(
        usamap, 
        idt,
        value = 'val',
        joinBy = c("hc-a2", "state"),
        borderColor = icolor,
        borderWidth = 0.2,
        nullColor = "#e3e8e8",
        allowPointSelect = TRUE,
        name = input$g_mapStat,
        states = list( 
            select = list( color = icolor, borderColor = 'black', borderWidth = 1, shadow = FALSE ),
            hover = list( color = icolor, borderColor = "#f1f3f3" )
        ),
        tooltip = iformat$tooltip
    )

    ichart %<>% 
        hc_chart(height=iheight) %>% 
        hc_colorAxis(
            min=min(idt$val,na.rm=T), max=max(idt$val,na.rm=T),
            minColor='#ffffff', maxColor = icolor, type=' '
        )
    
    progclose()

    return(ichart)

})