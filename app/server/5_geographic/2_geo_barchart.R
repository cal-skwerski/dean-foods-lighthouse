output$g_click_graph <-renderHighchart({ 
  
  proginit()

  geo_updateAll()

  # Fix business unit from Factor.
  idt <- click_dat()$data %>% filter( repCnt >= geo_minCohortCount( forTable = FALSE ) )
  ispct = gtypes[[input$g_mapStat]] == 'decimal' && max(idt$Statistic) <= 1

  iformat = gformats[[ gtypes[[ input$g_mapStat ]] ]]

  # Get the countrywide metric.
  cw_stat <- geo$cw_dat[, input$g_mapStat] %>% as.numeric()

  # If there is just one category, convert it to a list or highcharts will only show the first character.
  catgs = idt$catg
  if( length( catgs ) == 1 ) catgs = list(catgs) 

  ichart <- highchart() %>%
    hc_plotOptions( style = list(fontFamily='OpenSans') ) %>%
    hc_chart( height = chart_height - 70, marginRight = 60 ) %>%
    hc_colors( colors = color.medium( colorFor('g_map') ) ) %>% 
    hc_xAxis( title = list( enabled = FALSE ), categories = catgs ) %>%
    hc_yAxis(
      labels = list( enabled=F),
      gridLineWidth = 0,
      title = list(enabled=F,text = gsub("_", " ", input$g_mapStat)),floor = 0,
      minRange = if_else(ispct,100,if_else(input$g_mapStat == "Total Incurred", 0, min(as.numeric(cw_stat))*1.8)), # I just made this 1.75 becuase it fit the Country-Wide line on the graphs
      ceiling = if(ispct) {1}
    )

  if( input$g_mapStat != "Total Incurred" ){
    ichart %<>% hc_yAxis(
      plotLines = list(list(
          value = cw_stat, width = 2, 
          #color = '#adb5b8',
          color = 'rgba(173, 181, 184, .5)',
          zIndex = 9,# dashStyle = 'dash',
          label = list(
            text = paste0( 'Countrywide Ave: ', iformat[['formatter']](cw_stat) ),
            style = list( fontWeight = "normal", fontFamily = 'Rubik', fontSize = '10pt', color = '#5f6a6d', textOutline = '3px contrast' ),
            rotation = 0
          )
        ))
    )
  }

  if( nrow(idt) > 0 ) ichart %<>% hc_add_series( 
    idt$Statistic, type = 'bar', tooltip = iformat[['tooltip']], showInLegend = FALSE,
    name = input$g_mapStat,
    dataLabels=list(
      enabled = TRUE, 
      format = iformat[['pointFormat']],
      inside = FALSE,
      crop = FALSE,
      overflow = 'none',
      style=list(
        fontWeight='normal', 
        fontFamily='Rubik',
        color=color.medium( colorFor('g_map') ),
        fontSize='1.1em',
        textOutline='0px'
      )
    )
  )

  if(!is.null(input$selectedStates)) ichart %<>%  hc_subtitle( text = paste(as.character(input$selectedStates), collapse = ", ") )
    
  progclose()
  
  return(ichart)

})

click_dat <- reactive({
  
  idt <- g_dat_clickedStates()$data %>% group_by( exposureType, axYr, state, busUnit ) %>% sumNumeric() %>% ungroup() %>% gen_addExposure("Current Evaluation")
  
  # If multiple states (this includes no states, since that pulls all states) are clicked 
  if( length( unique( idt$state ) ) > 1 ){

    # If we aren't filtering by business unit, use that.
    if( nanull( isolate(input$busUnit) ) ){

      idt %<>% group_by(busUnit) %>% sumNumeric() %>% gmut()
      idt$catg <- paste(idt$busUnit)
      ilabel = 'Business Unit'

    # Otherwise use State.
    } else {
      idt %<>% group_by(state) %>% sumNumeric() %>% gmut()
      idt$catg <- idt$state
      ilabel = 'State' 
    }
    
  # If one or zero states are clicked, always use only business unit.
  } else {

      idt %<>% group_by(busUnit) %>% sumNumeric() %>% gmut()
      idt$catg <- paste(idt$busUnit)
      ilabel = 'Business Unit'
  }
  
  # Sort and return.
  colnames(idt)[colnames(idt)==input$g_mapStat] <- 'Statistic'
  idt %<>%  
    arrange( desc( Statistic ) ) %>% 
    top_n( 10, Statistic ) 

  # Remove null categories.
  idt %<>% filter( !is.na( catg ) )

  return( list(
    data=idt, state_filtered = g_dat_clickedStates()$state_filtered, label = ilabel
  ))
   
})