# Function for aggregating. From dataPrep.

getLine <- function(){

    # The fastest is if we already have modifyValues.
    if( 'line_modifyValues' %in% names( workdt[[coverage()]] ) ) {
         return( workdt[[coverage()]][['line_modifyValues']] )

    # Then check if line is loaded, and if so modify the values.
    } else if( 'line' %in% names( workdt[[coverage()]] ) ) { 
        workdt[[coverage()]][['line_modifyValues']] <<- claims_modifyValues( workdt[[coverage()]][['line']] ) # in case there are value modifications, run them.
        return( workdt[[coverage()]][['line_modifyValues']] )

    # And finally read in the claims data if we haven't done so yet.
    } else { 
        l( 'Reading RDS for ', coverage() )
        proginc( paste0( 'Reading ', coverage(),' Claims ~30 Seconds' ) )
        workdt[[coverage()]][['line']] <<- readRDS( paste0( 'data/coverage_', gsub('/','',coverage()), '.rds' ) ) # load the RDS into our data object.
        workdt[[coverage()]][['line_modifyValues']] <<- claims_modifyValues( workdt[[coverage()]][['line']] ) # in case there are value modifications, run them.
        return( workdt[[coverage()]][['line_modifyValues']] )
    }
}

# Get data type for the currenlty-active view.
dataType <- function(){
    itabset = isolate(input$tabset)
    if( itabset == 'trend' ) return(ifelse( isolate(input$trend_tabset) == 'byPeriod', 'byPeriod', 'byClaimYear' ))
    return(itabset) # something must be returned so we can track inputs and changes relative to data types. If none of the above special rules apply, just return the tab.
}

# Aggregation function. Matches aggregation in data prep.
reagg <- function() {

    proginc('Aggregate Data')

    # Get resources and skip if not applicable.
    idtype = dataType()
    ifilters = active_sidebarFilters()
    # No need for reagg if we already have the columns we need.
    # We must reagg if this data type aggregation is not currently available.
    if( idtype %in% names(workdt[[coverage()]]) ) if( length(ifilters) == 0 || all(ifilters %in% colnames(workdt[[coverage()]][[idtype]])) ) return() 

    l('Reaggregating...')

    # Get default reag columns that we'll always use.
    if( idtype == 'geographic' ){
        icols = c( 'busUnit', 'state', 'axYr', 'location', 'eval', 'exposureType', 'claimType' )
    } else if( idtype == 'byPeriod' ){
        icols = c( 'claimType', 'busUnit', 'state', 'eval', 'priorYr_eval', 'priorQt_eval', 'priorQt_change_bucket', 'priorYr_change_bucket', 'axPeriod', 'exposureType' )
    } else if( idtype == 'byClaimYear' ){
        icols = c( 'maturity', 'coverage', 'claimType', 'busUnit', 'state', 'exposureType', 'priorYr_eval', 'priorQt_eval', 'totInc_bucket', 'axPeriod', 'axYr', 'eval' )
    } else {
        stop('No aggregation necessary. reagg should not have been called.' )
    }

    # Update the aggregated data set for this data type.
    workdt[[coverage()]][[idtype]] <<- getLine() %>%
        group_by_at( unique( c( icols, active_sidebarFilters() ) ) ) %>%
        summarise_at( 
            vars(
                totPd, totInc, caseRes, clsCnt, openCnt, clm_count, repCnt,
                priorYr_totInc, priorYr_totPd, priorYr_clsCnt, priorYr_openCnt, priorYr_incDiff, priorYr_pdDiff, priorYr_repCnt,
                priorQt_totInc, priorQt_totPd, priorQt_clsCnt, priorQt_openCnt, priorQt_incDiff, priorQt_pdDiff, priorQt_repCnt
            ),
            funs(sum), na.rm = TRUE
        ) %>%
        ungroup()
        
}

sumNumeric <- function(idt) idt %>% summarise_if( is.numeric, sum, na.rm = TRUE ) %>% ungroup()

# Get current aggregated dataset.
getAgg <- function(){
    reagg() # perform reagg if necessary.
    return( workdt[[coverage()]][[dataType()]] )
}