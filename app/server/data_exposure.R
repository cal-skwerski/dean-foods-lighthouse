# Add exposure.
# Run this only after grouping data by state and business unit.
gen_addExposure <- function(iclaims, imaturity){
  
  # If there are multiple exposure types (excluding the 'Not Matched' type, and exposureType isn't applicable to this view, we can't add exposures.
  if(! 'exposureType' %in% colnames(iclaims) ) stop('gen_addExposure: exposureType was not found in the claims data.')
  if( length(setdiff(unique(iclaims$exposureType),'Not Matched')) != 1 && !filterApplies('exposureType') ) return(iclaims)

  # Check if we can even add exposure-based metrics. 
  # We'll do this by comparing the non-numeric fields (we assume these are the grouping/category fields) in claims and exposures.
  group_cols = colnames(iclaims)[ sapply( colnames(iclaims), function(x) !is.Date(iclaims[[x]]) && (is.factor(iclaims[[x]]) || is.character(iclaims[[x]]) ) ) ]

  # It's OK if we have columns that are single-valued. This happens in launch pad. Remove these from join_cols.
  join_cols = group_cols
  for( i in join_cols) if( !i %in% c('exposureType','axYr', 'claimType') && length(unique(iclaims[[i]]))<=1 ) join_cols = setdiff(join_cols,i)
  if( any( ! join_cols %in% colnames(exposures) ) ) return(iclaims)

  # We always need exposure type from claims. It's just safer to always have it.
  if( ! 'exposureType' %in% join_cols ) stop('gen_addExposure: exposureType was not found in the claims data group columns.')
  if( ! 'axYr' %in% join_cols ) stop('gen_addExposure: axYr was not found in the claims data group columns.')

  # Otherwise, we can add exposures! Summarize exposures using the same levels as claims.

  l('gen_addExposure')

  # Calculate in order to match claims for the latest maybe-partial year.
  # We always use the proration for maturity = Current Evaluation since geographic is limited to current eval.
  # This may change for other tabs.
  iexp = exposures %>% filter( 
      !is.na(exposure), exposure > 0, # Only positive exposure segments. It doesn't make sense to calc frequency on segments with no exposure.
      coverage == coverage() # Exposure dataset has multiple coverages so it must be filtered (unlike claims which have a unique dataset per coverage).
    ) %>% 
    applyFilters_specific() %>%  # apply sidebar filters.
    group_by_at( join_cols ) %>% summarize( exposure = sum(exposure) ) %>% ungroup()

  # Apply pro-ration based on maturity. 
  # TODO This seems to assume policy periods start in January. Double-check this..
  if( imaturity == "Current Evaluation" ){
      proration = lubridate::month( currentEval ) / 12
      iexp %<>% mutate( exposure = ifelse( axYr == endYr, exposure * proration, exposure ) ) # endYr is calculated in global.R.
  } else {
      proration = min( matToNumber( imaturity ) / 12, 1)
      iexp %<>% mutate( exposure = exposure * proration )
  }

  # Do the same with claims, just to be sure it is at the same summary level.
  iclaims %<>% group_by_at( group_cols ) %>% sumNumeric()

  # Check data.
  sumclaims = sum(iclaims$totInc) # we'll check this later to ensure it hasn't changed.

  # Merge exposure to claims.
  iclaims %<>% left_join( iexp, by = join_cols )

  # Exposures rarely match claims exactly. So there will be some NAs. Replace them with 0s.
  iclaims$exposure[is.na(iclaims$exposure)] <- 0

  # Validate that the total incurred did not change (it will if we haven't done our join correctly).
  if(sumclaims!= sum(iclaims$totInc)) stop('gen_addExposure error: Merged total incurred is higher than claims passed initially.' )

  return(iclaims)

}

# Function for calculating exposure metrics when exposures are available.
# Should only be used after final summary of volume metrics since we divide (not sum).
exposureMetrics <- function(idt){

  if( 'exposure' %in% colnames(idt) ){

    lrMult <- lp_exposureTypeInfo()[['Loss Rate Multiplier']]
    freqMult <- lp_exposureTypeInfo()[['Frequency Multiplier']]
    
    idt %<>% mutate( 
      `Incurred Loss Rate` = totInc / exposure * lrMult,
      `Paid Loss Rate` = totPd / exposure * lrMult,
      `Reported Frequency` = repCnt / exposure * freqMult
    )

  }
  
  return(idt)

}