# Server-side code for standard tables. See func/ui/table.R for the UI-side code.
table_server <- function( 

  data, currency_cols=c(), count_cols=c(), percentage_cols=c(), decimal_cols=c(), dollars_cols=c(), date_cols = c(),
  caption = NULL, order = NULL
  
){
    
    # Fix inputs.
    if( length(currency_cols)==0 && length(dollars_cols) > 0 ) currency_cols = dollars_cols # these are the same inputs with different names to help out users. this code will use currency_cols though.
    # if( nanull( order ) ){ order = list( list( 1, 'asc' ) ) }
    if( nanull( order ) ){ order = NULL }

    # Start table.
    idt <- datatable(
      data,
      rownames = F, extensions = "Buttons",
      caption = caption, 
      options = list(
          scrollX = TRUE, 
          columnDefs = list(
            list(className = 'right', targets = sort( unique( c( currency_cols, count_cols, percentage_cols, decimal_cols, date_cols ) ) - 1 ) ),
            list(className = 'left', targets = 0),
            list(orderSequence = c('desc', 'asc'), targets = "_all")
          ),
          pageLength = 15,
          order = order
      )
    )

    # Date formatting.
    if( length( date_cols ) > 0 ) for( i in date_cols ) idt %<>% formatDate( i, method = 'toLocaleDateString' )

    # Identify large dollars.
    large_dollars = c()
    for( i in currency_cols ) if( is.numeric(data[[i]]) && max( data[[i]], na.rm = T ) > 1000) large_dollars = c( large_dollars, i )
    small_dollars = setdiff( currency_cols, large_dollars )
    
    # Use DT functions. These functions get applied to the DataTable, not the data itself.
    if(length(large_dollars)>0) idt %<>% formatCurrency(columns=large_dollars,digits = 0)
    if(length(small_dollars)>0) idt %<>% formatCurrency(columns=small_dollars,digits = 2)
    if(length(count_cols)>0) idt %<>% formatCurrency(columns=count_cols,currency = "",interval = 3,mark = ",",digits = 0)
    if(length(percentage_cols)>0) idt %<>% formatPercentage(percentage_cols,1)
    if(length(decimal_cols)>0) idt %<>% formatRound(decimal_cols,2)
    
    # Return table.
    return(idt) 
    
}