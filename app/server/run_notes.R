# Function for logging run notes to clarify what is running when.
l <- function( ... ){
    if( is_local ) print( paste0( ... ) ) # only if local, we don't want to blow up the logs.
}