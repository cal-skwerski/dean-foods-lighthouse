# <<- must be used here since this object is shared between UI and server.
launch_labels <<- c(
  incSev = 'Incurred Severity',
  pdSev = 'Paid Severity',
  avgOpenCaseRes = 'Average Open Case Reserve',
  closingRate = 'Closing Ratio',
  clsRate = 'Disposal Ratio',
  incrClsRate = 'Incremental Disposal Ratio',
  incrIncSev = 'Incremental Incurred Severity',
  incrPdSev = 'Incremental Paid Severity',
  `Incurred Loss Rate` = 'Incurred Loss Rate',
  `Paid Loss Rate` = 'Paid Loss Rate',
  `Reported Frequency` = 'Reported Frequency'
)

# Options for the metric shown at geographic and their data types.
gtypes <<-c(
  'Average Incurred Severity' = 'dollars',
  'Total Incurred' = 'dollars',
  'Settlement Ratio' = 'decimal',
  #'Disposal Ratio' = 'decimal',
  'Closing Ratio' = 'decimal',
  'Average Open Case Reserve' = 'dollars',
  'Reported Counts' = 'count',
  'Incurred Loss Rate' = 'decimal',
  'Paid Loss Rate' = 'decimal',
  'Reported Frequency' = 'decimal'
)
if('state'%in%colnames(exposures)) gtypes = c( gtypes, c( # add frequency metrics, but only if we have state exposures.
  'Incurred Loss Rate' = 'decimal',
  'Paid Loss Rate' = 'decimal',
  'Reported Frequency' = 'decimal'
))

cda_metrics <<-c(
  "Incurred Severity" = "incSev", 
  "Paid Severity" = "pdSev", 
  "Average Open Case Reserve" = "avgCase",
  "Total Incurred" = "totInc", 
  "Total Paid" = "totPd", 
  "Total Open Case Reserve" = "caseRes",
  "Reported Count" = "repCnt", 
  "Open Count" = "openCnt"
)

# <<- must be used here since this object is shared between UI and server.
cda_volume <<- c( "repCnt", "openCnt", "totInc", "totPd", "caseRes" )