// Functions for building chart labels. Mirrors similar functions in R.
function chartLabel(){
    
    var i = this.y ?  this.y : this.total;

    return chartLabel_val(i);
}
function chartLabel_val(ival, aspercent, digits ){

    i = ival;
    absi = Math.abs(i);

    var divxby = absi > 1000000000 ? 1000000000 : ( absi > 1000000 ? 1000000 : ( absi > 20000 ? 1000 : 1 ));
    if(divxby == 1 && !aspercent ){
        if( i == 0 ){ return i.toFixed(0); }
        if( Math.floor(absi) == absi ){ return i.toFixed(  0  ); }
        if( absi < 10 && Math.floor(absi)!= absi ){ return i.toFixed( digits ? digits : ( absi < .2 ? 4 : 2) ); }
        return i.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    // Adjust to percentage or add suffix.
    if(aspercent){ i = (i * 100).toFixed(digits ? digits : 1) + "%"; 

    } else {
        
        // Divide and add the indicator (billion, million, thousand)
        var isuffix = absi > 1000000000 ? 'B' : ( absi > 1000000 ? 'M' : ( absi > 20000 ? 'K' : '' ));
        i = (i / divxby).toFixed(digits ? digits : 1);
        i = i + ' ' + isuffix; 
        
    }
    
    return i;

}

// Code to format as number with commas. 
// https://beta.rstudioconnect.com/barbara/format-numbers/
function localeString(x, sep, grp) {
    var sx = (''+x).split('.'), s = '', i, j;
    sep || (sep = ',');            // default separator
    grp || grp === 0 || (grp = 3); // default grouping
    i = sx[0].length;
    while (i > grp) {
      j = i - grp;
      s = sep + sx[0].slice(j, i) + s;
      i = j;
    }
    s = sx[0].slice(0, i) + s;
    sx[0] = s;
    return sx.join('.');
}