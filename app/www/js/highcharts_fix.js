/* Javascript fixes to problems with highcharts */

/* This function runs each time a visual is updated, with 'target' being the affected element. */
/* disabling this for now - it doesn't seem to fix the issue. 
var savethis;
$(document).on('shiny:recalculated',function(event){
    
    var t = $(event.target);

    // Highcharts or highcharter seems to want to redraw everything at height 400px. Undo this by clearing the height so it is determined by the parent element.
    if( 
        t.hasClass('highchart') // is a chart.
    ){
        savethis = t;
        t.removeClass('highchart');
        // Removing height fix, height isn't really an issue. But saving it so I dont' forget the issue with the map.
        //if( t.attr('id') != 'g_map' ){ // this breaks reactivity when the map runs.
        //     $(event.target).css('height', 'auto'); 
        //}

        //$(event.target).css('width', 'auto'); // width wants to be 100%, but auto works better.
        t.find('.highcharts-container').css('width', t.width()+'px');
        t.find('.highcharts-container').removeClass('highcharts-container');
        t.find('.highcharts-root').removeClass('highcharts-root');
        //var svg = t.find('svg.highcharts-root')[0];
        //if(svg){
        //    svg.setAttribute('width',t.width());
        //    svg.setAttribute('viewBox','0 0 ' + t.width() + ' ' + $(svg).height() );
        //}
        
    }

});*/

// Everything needs to happen after the document is ready.
// I tried lots of things to fix issues with charts making themselves smaller,
//   the solution was to add a static height in the R render function - even though it's static it'll still resize if it's container gets smaller.
//   So none of the below is necessary, but leaving it here in case it becomes useful later.
/*var t;
$(document).ready( function() {

    // Whenever a input is changed (in particular, a tabset) fix the charts.
    $('#trend_tabset > li').click(function(){        

        $( $(this).children('a').attr('href') ).find('.highcharts-container').each( function(){ $(this).css('width','auto'); });
        $( $(this).children('a').attr('href') ).find('.highcharts-root').each( function(){
            
            t = this;
            //var t = $('#byPeriod_incChangeGraph').find('.highcharts-root')
            
            var iparent = $(t).parent();
            if( !iparent.height() || !iparent.width() ){ return; } // if the chart is hidden, do nothing.

            // Get new height/width.
            var ih = Math.floor( iparent.height() );
            var iw = Math.floor( iparent.width() ) ;
            $(t).attr( 'width', iw );
            $(t)[0].setAttribute('viewBox','0 0 ' + iw + ' ' + ih );
            
        });

    });
});*/