
// Add tooltip functionality. See https://jqueryui.com/tooltip/.
$( function() { $( document ).tooltip(); } );

$( document ).ready(function() { // Run this code after all elements are loaded.
    
    $('.logo').html('<img style="float:left;padding:7px;width:auto;max-width:160px;padding-left:0;vertical-align:middle;" src=logo.png alt="Company Logo"/>'); // Set logo.
    
    // OWAC logo.
    $('#tabset').append('<div style="float:right;margin-right: 5px; text-align:right; margin-top: 7px; ">' +
        '<img src="ow-logo.png" style="width:100px;text-align:right;" alt="Oliver Wyman Actuarial Consulting Logo"/>' +
        '<p style="margin:0;padding:0;font-size:0.7em;" class="helper_subscript">&copy;' + (new Date()).getFullYear() + ' Oliver Wyman Actuarial Consulting</p>' +
    '</div>');

    // Allow reorder of UL elements.
    $(".sortable").sortable();
    $(".sortable").disableSelection();
    $('.sortable').sortable({ cancel: '.selectize-control, .selectize-dropdown-content, .chart_container' });
    
    // Special key handling.
    $(document).keydown(function(e){ 

        // Enter applies filters.
        if (e.keyCode == 13 ) {
            
            // if a sidebar input is selected, run apply.
            if( 
                $(document.activeElement).hasClass('shiny-bound-input') ||
                $(document.activeElement).parents().hasClass('shiny-bound-input') 
            ){ $('#applyButton').click(); } 

        // Ctrl + Alt + u downloads user log (only available for owac users).
        } else if( e.keyCode == 85 && e.ctrlKey && e.altKey){ $('#user_log_download_div').attr('style','');

    }}); 

    // title fix.
    $('title').html('Oliver Wyman Lighthouse');
    $('title').append('<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">');

});

// Function that runs any time a visual is shown.
$(document).on('shiny:inputchanged',function(event){

    // Set the title of the 2nd Geographic chart.
    $( '.active #g_click_graph_saveDiv .selectize-input.full').each(function(){ $(this).html($('#g_map_saveDiv .item').text()); });
    
    // Refresh the filter notes.
    $('.active .filter-note').each(function(){ $(this).html('<p class="helper_subscript">'+$('#filterNote').text()+'</p>'); });
    $('.active .filter-note-prevention').each(function(){ $(this).html('<p class="helper_subscript">'+$('#note_filter_prevention').text()+'</p>'); });
    $('.active .lp-filternote-comparedtoprior').each(function(){ $(this).html('<p class="helper_subscript">'+$('#lp_filternote_comparedtoprior').text()+'</p>'); });
    $('.active .filterNote_prev_wOverall').each(function(){ $(this).html('<p class="helper_subscript">'+$('#filterNote_prev_wOverall').text()+'</p>'); });
    
    
});
/*
$(document).on('shiny:conditional',function(event){ // This runs a lot, but after plots so it is the only way to check final plot status.
    
    // Add notes about no data available for charts.
    $('.active .highchart.html-widget.html-widget-output.shiny-bound-output').each( function(i, ielem ){

        ielem = $(ielem);
        ielem.parent().children('.no-data-alert').remove(); // remove any existing alerts.

        if( ielem.css('visibility') == 'hidden' ){
            ielem.before( '<p class="no-data-alert" >No data available for this chart. Please try a different selecton.</p>' ); // if applicable, add one.
            //$(this).css('display','none'); // For some reason, chart elements are coming through when visibility is hidden, this fixes that.
        }

    });

});
*/