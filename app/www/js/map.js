$( document ).ready(function() { // Run this code after all elements are loaded.
    // Set up onclick function for map
    $('#g_map').click( function() {
        var selected_states = $('#g_map').highcharts().getSelectedPoints().map(function(x) {return x.code;});
        Shiny.onInputChange('selectedStates', selected_states);
    }); 
});