function lp_filter( colnames, values, metric_name ){
    
        // Clear all existing sidebar selections.
        $('#sidebarInputs_div select.shiny-bound-input').each( function(i,iselect){
            $(iselect)[0].selectize.clear();
        });
    
        // Set maturity to the default used by Spotlight.
        //$('#maturity')[0].selectize.setValue('12 Months');
    
        // Set the new selections.
        colnames = colnames.split('_x_');
        values = values.split(' & ');
        for(var i=0; i < colnames.length; i++){
            var iselectize = $('#'+colnames[i])[0].selectize;
            iselectize.addOption({value: values[i], text: values[i]});
            iselectize.refreshOptions();
            iselectize.addItem(values[i]);
        }
    
        $('.selectize-dropdown').hide(); // this operation opens the drop-down, hide it so it isn't distracting.
    
        // Set the clicked metric.
        $('#launch_execute_clickedMetric')[0].selectize.setValue(metric_name);
        
        // Execute the launch click operation to trigger the reactive (which will trigger tab change and data load).
        $('#launch_execute').click();
    
    }
    
    function lp_showDetail(event,j){
        $('#lp-expand-'+j).show();
        $(event.target).hide();
    }