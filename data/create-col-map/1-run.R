# This code helps with building column maps from loss runs to default R.
# The actual mapping is defined in the sister file. When finished, this will output a CSV file that can be used to fill out setup.xlsx.

# How it works: fill out Files sheet in setup to give R the file structure, it'll read in the column names and data types.
# It will tell you the next missing mapping. Add the relevant info to 0-edit.R and run the code again, it'll tell you the next column that needs to be mapped.
# Keep running the code until you have mapped all the columns or identified which columns aren't in specific files, at which point it will write a csv of the map in this folder.
# Dataprep.R doesn't quite read data in in that format yet, but this will be the case eventually.

# Set up workspace.

  require(owactools)
  owactools::usePackage(c('sqldf','dplyr','lubridate','magrittr','reshape2'))
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  setwd('../..') # This goes to the next 2 levels up, where setup.xlsx is. From here we can reach all the files we need.


# If we don't have colmap0 (information about data columns), read it in.
  
  # if(exists('colmap0')) rm(colmap0) # run this line to trigger a re-read of the files.
  if( !exists('colmap0') ){
    
    rm(list=ls(all=TRUE)) # Housekeeping
    
    # Get file locations.
    files = read.any( 'setup.xlsx', sheet = 'Files', first_column_name = 'File Location (absolute or relative to the location of dataPrep.R)' )
    for( i in 1:nrow(files) ){
      
      cat( 'Processing ', paste0( 'data/', files$`File Location (absolute or relative to the location of dataPrep.R)`[i] ), '\n' )
      
      # Attempt file read, but just the first few rows since we just want the structure.
      idt = read.any( 
        paste0( 'data/', files$`File Location (absolute or relative to the location of dataPrep.R)`[i] ), 
        sheet = files$`Sheet (Name or Number, blank = 1)`[i],
        first_column_name = files$`First Column Name`[i],
        headers_on_row = files$`OR Header Row`[i],
        nrows = 1000 # needs to be enough to check for all NAs.
      )
      
      idt <- idt[ , sapply( idt, function(icol) !all(is.na(icol)) ) ] # remove all-NA columns.
      
      # Gather column information.
      colinfo = data.frame(
        colnames = colnames(idt), 
        type = ifelse( 
          sapply( idt, function(icol) likedate(icol) || is.Date(icol) ),
          'date',
          ifelse( 
            sapply( idt, function(icol) char_num(icol) || is.numeric(icol) ),
            'num',
            'char'
          )
        ),
        vals = sapply( idt, function(icol) paste( smpl(icol), collapse = ', ' ) ),
        file = rep( files$`File Location (absolute or relative to the location of dataPrep.R)`[i], ncol(idt) ),
        stringsAsFactors = FALSE
      )
      
      # Start or add this to colmap0.
      if( !exists('colmap0') ){ colmap0 = colinfo } else { colmap0 = bind_rows( colmap0, colinfo ) }
  
      rm( idt, i, colinfo ) # housekeeping.
      
    }
    
    rm(files) # houskeeping.
    
  }

  colmap = colmap0
  colmap$matchname = gsub( '[^a-z0-9]', '', tolower( trimws( colmap$colnames) ) )
  colmap$rname <- NA

# Validate column names are not duplicated within files.
  
  dupnames = colmap %>% group_by( file, matchname ) %>% summarize( n = n() ) %>% filter( n > 1 )
  if( nrow(dupnames) > 0 ){
    print( dupnames )
    stop( 'Duplicate column(s) found. This will likely cause issues downstream.')
  }
  rm(dupnames)

# Define map from rname to match-on (processed from excel) names.
  source('0-edit.R') # open this file and change it as you loop through this code.

# For each file, check for each column in the standard column map.
# When a column is not found, add it to the list of not-found columns.
  
  filelist = unique( colmap$file )
  if(exists('miscols')) rm(miscols)
  for( jfile in filelist ){
    
    # Get the file's specific columns.
    jcolmap = colmap %>% filter( file == jfile )
    
    # Check for each standard column.
    for( l in 1:length(cols) ){
      
      # Is this column in the file?
      if( jfile %in% cols[[l]]$notin ) next
      
      foundmatch = FALSE
  
      # Check for each match value, stopping when a match is found.
      for( mmatchon in cols[[l]]$matchon ){
        
        # Is the current match on column a column in the data?
        # If there is match, set the rname value in the original dataset, colmap.
        # There should only ever be one match, since we have already checked for unique column names.
        if( any(jcolmap$matchname==mmatchon) ){
          
          jcolmap %<>% filter( matchname != mmatchon ) # so future loops don't check this one (matched already) again.
          colmap$rname[ colmap$matchname == mmatchon & colmap$file == jfile ] <- cols[[l]]$rname
          
          foundmatch = TRUE # identify a match was found.
          
          # clean house and skip to the next column type/rname.
          rm(mmatchon)
          break 
        }
  
        rm( mmatchon ) # housekeeping
      
      }
      
      # If a match was not found, add to the non-matched columns.
      if(!foundmatch){
        
        # Create a row.
        nminfo = data.frame(
          file = jfile,
          rname = cols[[l]]$rname,
          req = if( isval( cols[[l]]$req ) ){ cols[[l]]$req } else { FALSE },
          type = cols[[l]]$type,
          stringsAsFactors = FALSE
        )
        
        # Create or bind to missing columns.
        if(!exists('miscols')) { miscols = nminfo } else { miscols = bind_rows( miscols, nminfo ) }
        
        rm( nminfo ) # housekeeping
        
      }
      
      rm( l, foundmatch ) # housekeeping
    }
    rm(jcolmap,jfile) # housekeeping
  }

# Validate each column only shows up once per file.
  
  colcounts = colmap %>% filter( !is.na(rname) ) %>% group_by( file, rname ) %>% summarize( n = n() ) %>% filter( n > 1 )
  if(nrow(colcounts)>0){
    print(colcounts)
    stop( 'Duplicate columns will be created. Please correct mappings.' )
  }
  rm(colcounts) # houskeeping.

#  Check for required values.
  
  if( !exists('miscols') || any(!is.na(miscols$req)) ) cat( 'Success! All required columns have been identified in each dataset.\n' )

# Check for all values.
  
  if( !exists('miscols') ){ 
  
    # If we have all the values, alert and write the column map which can be used in setup.xlsx.
    cat( 'Success! You have created a full column map which can be applied to these loss runs.' )
    w( 
      colmap %>% filter( !is.na(rname) ) %>% select(
        `Dashboard R Name` = rname, File = file, `Excel Column Name` = colnames
      ) %>% unique(),
      'data/create-col-map/colmap'
    )
  
  # Otherwise, pick the first missing value and print information required to add it to colmap.
  } else {
    
    cat( 'first missing column: (of', nrow(miscols), '): \n' )
    cat( 'file:', miscols[1,'file'], '\n[', miscols[1,'rname'], '] req:', miscols[1,'req'], ' type:', miscols[1,'type'], '\n possible matches:\n' )
    cat(' Instructions: Select a matchname and add it to cols above, or add this file to "notin" in cols.\n' )
    
    # Calculate potential matches based on data type.
    print( colmap %>% filter( 
      type %in% ifelse( miscols[1,'type'] == 'charnum', c('char','num'), miscols[1,'type'] ), 
      file == miscols[1,'file'], 
      is.na(rname) 
    ) %>% mutate( vals = left(vals,50 ) ) %>% select( matchname, type, vals )
    )
    
  }
