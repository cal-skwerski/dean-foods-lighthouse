# Load Paths & Settings and a function for accessing them.
paths_settings <- read.any(filename='setup.xlsx',sheet='Paths Settings',first_column_name='Name')
setting <- function(settingName) paths_settings$Value[eq(paths_settings$Name,settingName)]
StartDay = setting('StartDay')

# Load Columns.
cols <- read.any( filename = 'setup.xlsx', sheet='Columns',first_column_name='Dashboard R Name' )
cols <- cols[ !is.na(cols$`Excel Column Name`), ]
cols$`Excel Column Name` <- cols$`Excel Column Name` # To match colnames read in by R.
field_name_map <- cols$`Dashboard R Name`
names(field_name_map) <- cols$`Excel Column Name`

mineval = setting('MinEvalDate')
if(!nanull(mineval)){
  mineval <- xldate(mineval)
  mineval_minusOne = mineval - lubridate::years(1) # 1 year back so we get prior evaluations for change.
}