
# Function for checking control totals.
checkControl <- function(value,name){
  expected <- round(as.numeric(setting(name)),2)
  if(is.na(expected)){ 
    cat(name,'does not have a expectation for control totals. \n')
  } else {
    value <- round(value,2)
    owactools::check_equal(expected=expected,actual=value,desc=name)
  }
}