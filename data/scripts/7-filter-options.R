# Get a consolidated table of unique options which will allow faster option identification.
uniqueOptions <<- function( LossRuns, sidebarInputs ){
  
  ilist <- list()
        
  havecols = c( colnames( LossRuns$dim ), colnames( LossRuns$val ) )
  names(havecols) <- havecols
  for( i in havecols ) havecols[i] <- ifelse( i %in% colnames( LossRuns$dim ), 'dim', 'val' )

  for( i in unique( LossRuns$dim$coverage ) ){ # get options for each available coverage.
    
    ilist[[i]] <- list()

    idt = list()
    idt$dim = LossRuns$dim %>% filter( coverage == i ) # filter dim.
    idt$val = LossRuns$val %>% inner_join( idt$dim %>% select( claim_id ), by = 'claim_id' ) # filter val.

    for( j in sidebarInputs$api_name ){

      # get the data set (val/dim) that the column in question is in.
      jcolin = havecols[ which( names(havecols) == j ) ]
      
      # Add unique values for the sidebar column.
      ilist[[i]][[j]] <- unique( idt[[jcolin]][[j]][ !is.na( idt[[jcolin]][[j]] ) ] ) 

    }

    rm(idt, j, jcolin ) # housekeeping.
    
  }
  
  return( ilist )
  
}