launch_top_movers <- function(launch_line){ 
  
  l('launch_top_movers')
  idt = launch_line %>% filter( eval == currentEval )
  
  ilaunch_top_movers <<- list(
    move_pd = idt %>% 
      filter( !is.na(prior_pdDiff), totPd > 0 ) %>% 
      top_n( 10, prior_pdDiff ) %>% 
      arrange( desc(prior_pdDiff) ) %>% 
      mutate( label = claimNum ),
    move_inc = idt %>% 
      filter( !is.na(prior_incDiff), totInc > 0 ) %>% 
      top_n( 10, prior_incDiff ) %>% 
      arrange( desc(prior_incDiff) ) %>% 
      mutate( label = claimNum ),
    new_inc = idt %>% 
      filter( is.na(prior_totInc), totInc > 0 ) %>% 
      top_n( 10, totInc ) %>% 
      arrange( desc(totInc) ) %>% 
      mutate( label = paste0( claimNum, ' @ ', gformats$date$formatter(lossDt) ) )
  )
  
  return(ilaunch_top_movers)
  
}

gformats <<- list(
  dollars = list(
    tooltip = list(valueDecimals = 0,valuePrefix = '$'), # for easily setting up tooltips in highcharter.
    format="${value:,.0f}",
    pointFormat = '{point.y:,.0f}',  # also for formatting in highcharts.
    formatter = function(x) scales::dollar(round(x,0)) # for any instance.
  ),
  percentage = list(
    tooltip = list(valueDecimals = 1,valueSuffix='%'),
    format="{value}%",
    pointFormat='{point.y:.0f}%',
    formatter = function(x) paste0(round(x*100,0),'%')
  ),
  decimal = list(
    tooltip = list(valueDecimals = 3),
    format="{value:.3f}",
    pointFormat='{point.y:.3f}',
    formatter = function(x) format(x,digits=3)
  ),
  count = list(
    tooltip = list(valueDecimals = 0),
    format="{value:,.0f}",
    pointFormat='{point.y:,.0f}',
    formatter = function(x) gsub('[$]','',scales::dollar(round(x,0)))
  ),
  date = list(
    formatter = function(x) gsub( '^0', '', format(x, '%m/%d/%Y') )
  )
)
